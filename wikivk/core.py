import json
from os.path import join, dirname
from lark import Lark, Tree, Transformer, v_args
from lark.reconstruct import Reconstructor

with open(join(dirname(__file__), 'grammar.lark')) as grammar_file:
    grammar = grammar_file.read()


parser = Lark(
    grammar,
    start="html",
    lexer="dynamic_complete"
)


def convert_html(text):
    parse_tree = parser.parse(text)
    # print(parser.parse(text).pretty())
    # print(parse_tree)

    return EvalExpressions().transform(parse_tree)
    # print(ttt)


@v_args(inline=True)
class EvalExpressions(Transformer):
    '''
    Трансформация таблиц
    Возможно вложение таблицы в таблицу.
    Возможно задать ширину колонок, напр. так: |~ 50% 50%
    '''
    columns_width = []  # напр. "25% 60% 15%" - храним ширину колонок таблицы
    td_count = 0        # считаем TD чтобы установить ширину только для первых
    li_count = 0

    def html(self, *all_together):
        return ''.join(all_together)

    def table(self, *trows):
        return '<table>' + ''.join(trows) + '</table>'

    def tdwidth(self, *colwidths):
        '''
        Сохраняем размеры колонок в атрибут self.columns_width[]
        '''
        for w in colwidths:
            self.columns_width.append(str(w)[1:])
        return ''

    def tr(self, *tcells):
        return '<tr>' + ''.join(tcells) + '</tr>'

    def td(self, *text_or_html):
        if self.td_count < (len(self.columns_width)):
            '''
            Устанавливаем <td width="NNN%"> для первой строки таблицы
            '''
            td_width = self.columns_width[self.td_count]
            td_tag = '<td width="{}">'.format(td_width)
            self.td_count += 1
        else:
            '''
            Для остальных ячеек таблицы ширину не ставим
            '''
            td_tag = '<td>'

        return td_tag + ''.join(text_or_html) + '</td>'

    '''
    Трансформация списков <ul>, <ol>
    '''
    list_level = 1  # For <ul> and <ol>

    def bullet_list(self, *result):
        return ''.join(result)

    def ul_first(self, token, data):
        return '<ul><li>' + data

    def ul_last(self, *token):
        out = ''
        end = '</li></ul>'
        while self.list_level > 0:
            out += end
            self.list_level -= 1
        return out

    def bullet_item(self, token, text):
        list_level = len(token)

        if self.list_level == list_level:
            out = '</li><li>' + text

        elif self.list_level < list_level:
            out = '<ul><li>' + text

        elif self.list_level > list_level:
            out = '</li></ul></li><li>' + text

        self.list_level = list_level
        return out

    def title(self, token1, text, token2):
        header_level = len(token1)
        return '<h{}>{}</h{}>'.format(header_level, text, header_level)

    def strong(self, text):
        return '<strong>{}</strong>'.format(text)

    '''
    TODO
    <i>, <u>, <a>, <img>
    html-теги, которые разрешены во ВКонтакте
    '''


'''
if __name__ == '__main__':
    convert_html(text)
    # main()
'''
