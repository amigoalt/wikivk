from setuptools import setup, find_packages
from os.path import join, dirname
import wikivk

setup(
    name='wikivk',
    version=wikivk.__version__,
    packages=find_packages(),
    long_description=open(join(dirname(__file__), 'README.rst')).read(),
    install_requires=[
        'lark-parser'
    ],
    author='Anton Gavrilyuk',
    author_email='amigoalt@gmail.com',
    url='https://bitbucket.org/amigoalt/wikivk',
    license='MIT',
    include_package_data=True,
    test_suite='tests.run',
)
