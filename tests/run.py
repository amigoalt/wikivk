from unittest import TestCase
from wikivk.core import convert_html


class wikivkTestCase(TestCase):
    def test_table(self):
        wiki, html = read_files('tests/table.mw', 'tests/table.html')
        self.assertEqual(convert_html(wiki), html)

    def test_ul(self):
        wiki, html = read_files('tests/ul.mw', 'tests/ul.html')
        self.assertEqual(convert_html(wiki), html)

    def test_strong(self):
        wiki, html = read_files('tests/strong.mw', 'tests/strong.html')
        self.assertEqual(convert_html(wiki), html)

    def test_title(self):
        wiki, html = read_files('tests/title.mw', 'tests/title.html')
        self.assertEqual(convert_html(wiki), html)


def read_files(wikifile_path, htmlfile_path):
    with open(wikifile_path) as mediawiki_file:
        wiki = mediawiki_file.read()
    with open(htmlfile_path) as html_file:
        html = html_file.read()
    return (wiki, html)
